// ----------------------------------------------------------------------------
// Copyright (C) 2024
//              Phil Taylor, M0VSE
//
// This file is part of fldigi
//
// fldigi is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// fldigi is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------

#ifndef	TCI_H
#define TCI_H

#include <iosfwd>
#include <string>

#include "threads.h"

extern void tci_setqsy(unsigned long long rfc);


extern void tci_close();
extern bool tci_init(bool bPtt);
extern void tci_setptt(int);
extern void tci_setfreq(unsigned long long f);
extern void tci_setmode(const char *);
extern void tci_getmode();
extern int tci_getsamplerate();
//extern void tci_setwidth(pbwidth_t);
//extern void tci_getwidth();
extern void tci_audio_flush(unsigned);
extern int tci_audio_read(float* data, size_t len);
extern int tci_audio_write(float* data, size_t len);
extern void tci_start_audio();
extern void tci_stop_audio();

#endif // TCI_H
