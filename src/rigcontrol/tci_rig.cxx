// =====================================================================
//
// tci_rig.cxx
//
// connect to TCI server
//
// Copyright (C) 2024
//		Phil Taylor, M0VSE
//
// This file is part of fldigi.
//
// Fldigi is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Fldigi is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with fldigi.  If not, see <http://www.gnu.org/licenses/>.
// =====================================================================

#include <iostream>
#include <chrono>
#include <math.h>
#include <cmath>
#include <cstring>
#include <ctype.h>
#include <vector>
#include <list>
#include <regex>
#include <stdlib.h>

#include <FL/Fl.H>
#include <FL/filename.H>
#include <FL/fl_ask.H>

#include "rigsupport.h"
#include "modem.h"
#include "trx.h"
#include "fl_digi.h"
#include "configuration.h"
#include "main.h"
#include "waterfall.h"
#include "macros.h"
#include "qrunner.h"
#include "debug.h"
#include "status.h"
#include "squelch_status.h"
#include "icons.h"
#include "easywsclient.h"

LOG_FILE_SOURCE(debug::debug::LOG_RIGCONTROL);

using easywsclient::WebSocket;

//======================================================================
// tci TCI client
//======================================================================


// Audio


#define TCI_AUDIO_LENGTH 4096

static constexpr unsigned int iqHeaderSize = 16u*sizeof(unsigned int);
static constexpr unsigned int iqBufferSie = 2048u;

typedef enum
{
	IqInt16 = 0,
	IqInt24,
	IqInt32,
	IqFloat32,
	IqFloat64
} tciIqDataType;

typedef enum
{
	IqStream = 0,
	RxAudioStream,
	TxAudioStream,
	TxChrono,
} tciStreamType;

typedef struct
{
	unsigned int receiver;
	unsigned int sampleRate;
	unsigned int format;
	unsigned int codec;
	unsigned int crc;
	unsigned int length;
	unsigned int type;
	unsigned  reserv[9];
	float   data[TCI_AUDIO_LENGTH];
} tciDataStream;


pthread_t *tci_thread = 0;
pthread_mutex_t mutex_tci = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_tci_rx = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_tci_tx = PTHREAD_MUTEX_INITIALIZER;

//static bool tci_exit = false;
static bool tci_ptt = false;
//static bool tci_closed = true;

//static long int tci_freq;

void movFreq(Fl_Widget *w, void *d);
void show_freq(void *);
void tci_getvfo();
void tci_getfreq();
void tci_setfreq(unsigned long long f);
void tci_getmode();
void tci_getbw();
void tci_setnotch(int);

bool tci_getxcvr();
void tci_connection();
void set_ptt();

void connect_to_tci();

static WebSocket::pointer ws = NULL;

std::vector<float> tci_rxbuf;
std::vector<float> tci_txbuf;
std::vector<std::string> tci_modes;

//----------------------------------------------------------------------
// declared as extern in rigsupport.h
//----------------------------------------------------------------------
static bool connected_to_tci = false;
static bool tci_audio_started = false;
//----------------------------------------------------------------------

//static bool bws_posted = false;
//static bool modes_posted = false;
//static bool freq_posted = true;

//static std::string xcvr_name;
//static std::string str_freq;
//static std::string mode_result;


static unsigned long long tci_freq;
static rmode_t tci_rmode = RIG_MODE_USB;
//static pbwidth_t tci_pbwidth = 3000;

static std::string tci_server;
static double tci_version;
static std::string tci_rig;
static int audio_samplerate=48000;
static bool audio_required = false;

//static double timeout = 5.0;

///static int wait_bws_timeout = 0;

//static unsigned long long fr_show = 0;

// Buffer for holding replies to TCI server
static char buff[255];


static void *tci_thread_loop(void *args);

int tci_getsamplerate()
{
	return audio_samplerate;
}

//======================================================================

void tci_setqsy(unsigned long long rfc)
{
	tci_setfreq(rfc);
	wf->rfcarrier(rfc);
	wf->movetocenter();
	show_frequency(rfc);
#ifdef __WIN32__
// this subterfuge is necessary due to a bug in mingw gcc macro parser
	char dummy[50];
	snprintf(dummy, sizeof(dummy), "set qsy: %llu", rfc);
	LOG_VERBOSE("%s", dummy);
#else
	LOG_VERBOSE("set qsy: %llu", rfc);
#endif
}

//======================================================================
// set / get pairs
//======================================================================

//----------------------------------------------------------------------
// push to talk
//----------------------------------------------------------------------
//static int  ptt_state = 0;

//static int  new_ptt = -1;


bool tci_init(bool bPtt)
{
	
	tci_freq = 0;
	tci_rmode = RIG_MODE_NONE;
	
	connect_to_tci();

	if (ws != NULL)
	{
		tci_thread = new pthread_t;

		if (pthread_create(tci_thread, NULL, tci_thread_loop, NULL) < 0) {
			LOG_ERROR("Cannot create TCI thread");
			ws->close();
			tci_thread = 0;
			return false;
		}

	} else {
		LOG_ERROR("TCI connection failed");
		return false;
	}
	
	init_tci_RigDialog();
	return true;
}

void tci_close()
{
	if (ws != NULL && connected_to_tci){
		ws->close();
		delete ws;
		ws = NULL;
	}
}

bool tci_active()
{

	return connected_to_tci;
}

void tci_setptt(int on) {
	if (ws != NULL && connected_to_tci) {
		guard_lock tci_lock(&mutex_tci);
	  	snprintf(buff, sizeof(buff), "TRX:0,%s;",(on==1)?"true":"false");
		ws->send(buff);
	}
}

pthread_mutex_t mutex_tci_ptt = PTHREAD_MUTEX_INITIALIZER;


void tci_getptt()
{
	if (ws != NULL  && connected_to_tci) {
		guard_lock tci_lock(&mutex_tci);
		ws->send("TRX:0;");
	}
}

void tci_updateWPM(void *)
{
        set_CWwpm();
}

void tci_getwpm(void) {
	return;
}

void tci_setwpm(unsigned char wpm) {

}


void tci_setfreq(unsigned long long f) {
	if (ws != NULL && connected_to_tci) {
		guard_lock tci_lock(&mutex_tci);
	  	snprintf(buff, sizeof(buff), "VFO:0,0,%lld;",f);
		ws->send(buff);
	}
}

void tci_getfreq() {
	if (ws != NULL && connected_to_tci) {
		guard_lock tci_lock(&mutex_tci);
		ws->send("VFO:0,0;");
	}
}

void tci_setmode(const char *md) {
	if (ws != NULL && connected_to_tci) {
		guard_lock tci_lock(&mutex_tci);
	  	snprintf(buff, sizeof(buff), "MODULATION:0,%s;",md);
		ws->send(buff);
	}	
}


void tci_getmode() {
	if (ws != NULL && connected_to_tci) {
		guard_lock tci_lock(&mutex_tci);
		ws->send("MODULATION:0;");
	}
}

void tci_setbw(int bw2, int bw1) {
	guard_lock tci_lock(&mutex_tci);
}

void tci_getbw() {
	guard_lock tci_lock(&mutex_tci);
}

//----------------------------------------------------------------------
// transceiver get / set vfo A / B
//----------------------------------------------------------------------

void tci_setab(int n) {
	guard_lock tci_lock(&mutex_tci);
}

void tci_showA(void *) {
	guard_lock tci_lock(&mutex_tci);
}

void tci_showB(void *) {
	guard_lock tci_lock(&mutex_tci);
}

void tci_getvfo() {
	guard_lock tci_lock(&mutex_tci);
}

//==============================================================================
// transceiver set / get notch
//==============================================================================
void tci_setnotch() {
	guard_lock tci_lock(&mutex_tci);
}

void tci_getnotch() {
	guard_lock tci_lock(&mutex_tci);
}

//==============================================================================
// transceiver get smeter
// transceiver get power meter
//==============================================================================
void tci_getsmeter() {
	guard_lock tci_lock(&mutex_tci);
}

void tci_getpwrmeter() {
	guard_lock tci_lock(&mutex_tci);
}
//==============================================================================
// transceiver get name
//==============================================================================

//static void tci_show_xcvr_name(void *)
//{
	//xcvr_title = xcvr_name;
	//setTitle();
//}

bool tci_getxcvr()
{
	return true;
}

//static bool run_tci_thread = true;
static int poll_interval = 1000; // 1 second

void tci_setQSY(void *)
{
	if (wf != NULL && connected_to_tci) {
		wf->setQSY(true);
	}
}

void tci_connection()
{
	connect_to_tci();
}

void connect_to_tci()
{
    guard_lock tci_lock(&mutex_tci);

	try {
  		snprintf(buff, sizeof(buff), "ws://%s:%ld/",
			progdefaults.tci_ip_address.c_str(),
			atol(progdefaults.tci_ip_port.c_str()));

  		std::string buffAsStdStr = buff;
		ws = WebSocket::from_url(buffAsStdStr);
		if (ws == NULL)
		{
			LOG_VERBOSE("Failed to connect to ws://%s:%ld/",
				progdefaults.tci_ip_address.c_str(),
				atol(progdefaults.tci_ip_port.c_str()));
			return;
		}

		LOG_VERBOSE("created TCI client to ws://%s:%ld/",
				progdefaults.tci_ip_address.c_str(),
				atol(progdefaults.tci_ip_port.c_str()));

		if (ws->getReadyState() == WebSocket::CLOSED) {
			connected_to_tci = false;
		}

	} catch (...) {
		LOG_ERROR("Cannot create TCI client to ws://%s:%ld/",
				progdefaults.tci_ip_address.c_str(),
				atol(progdefaults.tci_ip_port.c_str()));
	}
}

// Case insensitive string search.
bool strEquals(const std::string &a, const std::string &b)
{
	return std::equal(a.begin(), a.end(), b.begin(), b.end(), [](char a, char b) {
		return std::tolower(a) == std::tolower(b);
	});
}

void tci_message(const std::string & message)
{
	//LOG_INFO("Got cmd: %s", message.c_str());

	size_t curpos = message.find(':');
	size_t newpos = curpos;
	std::string cmd="";
	std::string arg[255];
	int count=0;
	if (curpos != std::string::npos) {
		cmd = message.substr(0,curpos);
		curpos++; //increment curpos to remove colon
		//LOG_INFO("Found cmd: %s", cmd.c_str());
		while (count < 254 && newpos != std::string::npos) {
			newpos = message.substr(curpos).find(',');
			if (newpos != std::string::npos) {
				arg[count]=message.substr(curpos,newpos);
				//LOG_INFO("Found arg: %s", arg[count].c_str());
				curpos=curpos+newpos+1;
			} else {
				break;
			}
			count++;
		}
		// Strip off terminating semicolon.
		newpos = message.substr(curpos).find(';');
		if (newpos != std::string::npos)
		{
			arg[count]=message.substr(curpos,newpos);
			//LOG_INFO("Found arg: %s", arg[count].c_str());
		} else {
			LOG_ERROR("Received data did not contain a ; %s (%d)", message.c_str(), message.length());
			return;
		}
	} else {
		newpos = message.find(';');
		if (newpos != std::string::npos) {
			cmd = message.substr(0,newpos);
			//LOG_INFO("Found command: %s", cmd.c_str());
		} else {
			LOG_ERROR("received data did not contain a : or ; %s", message.c_str());
			return;
		}
	}
	if (cmd != "")
	{
		if (strEquals(cmd,"ready"))
		{
			LOG_INFO("TCI is ready!");
			connected_to_tci = true;
			if (audio_required) {
				ws->send("AUDIO_START:0;");
			}
		}
		else if (strEquals(cmd,"protocol")) 
		{
			tci_server = arg[0];
			tci_version = atof(arg[1].c_str());
			LOG_INFO("Server: %s, version: %f", tci_server.c_str(), tci_version);			
		}
		else if (strEquals(cmd,"device")) 
		{		
			snprintf(buff, sizeof(buff), "%s (%s)", tci_server.c_str(), arg[0].c_str());
			std::string buffAsStdStr = buff;
			LOG_INFO("Rig Model is: %s", arg[0].c_str());
			xcvr_title = buffAsStdStr;
			setTitle();
		}
		else if (strEquals(cmd,"modulation") && count > 1)
		{
			uchar receiver = atol(arg[0].c_str());
			if (receiver == 0) {
				LOG_DEBUG("Received mode %s", arg[1].c_str());
				for (size_t i=0;i<tci_modes.size();i++)
				{
					if (strEquals(arg[1],tci_modes[i])) {
						qso_opMODE->index(i);
						break;
					}
				}
			}
		}
		else if (strEquals(cmd,"modulations_list"))
		{
			qso_opMODE->clear();
			tci_modes.clear();
			for (int i = 0; i <= count; i++) {
				qso_opMODE->add(arg[i].c_str());
				tci_modes.push_back(arg[i].c_str());
			}			
			qso_opMODE->index(2);
			qso_opMODE->activate();
		}
		else if (strEquals(cmd,"rx_smeter") && count > 1 && smeter)
		{
			uchar receiver = atol(arg[0].c_str());
			if (receiver == 0) {
				int s = atoi(arg[2].c_str());
				s=s+120; // (value is 0-100);
				if (smeter) {
					smeter->value(s);
				}
			}
		}
		else if (strEquals(cmd,"audio_samplerate"))
		{
			audio_samplerate = atoi(arg[0].c_str());
			LOG_INFO("audio_samplerate: %d", audio_samplerate);			
		}
		else if (strEquals(cmd,"vfo") && count >=2)
		{
			uchar receiver = atol(arg[0].c_str());
			uchar vfo = atol(arg[1].c_str());
			unsigned long long f = atoll(arg[2].c_str());
			LOG_INFO("RX:%d VFO:%d Freq: %lld", receiver, vfo, f);

			if (receiver == 0 && vfo == 0 && f != 0) {
				try {
					
					tci_freq = f;
					qsoFreqDisp->value(tci_freq);
					wf->rfcarrier(tci_freq);
				} catch (...) {
					LOG_ERROR("Error setting freq: %lld", f);
				}
			}
		}
		else if (strEquals(cmd,"audio_start")) 
		{
			tci_audio_started=true;
		}
		else if (strEquals(cmd,"trx")) 
		{
			uchar receiver = atol(arg[0].c_str());
			if (receiver == 0) {
				if (strEquals(arg[1],"true"))
					tci_ptt=true;
				else
					tci_ptt=false;
			}
		}
		else if (strEquals(cmd,"start"))
		{
			// Don't do anything for these commands.
		}
		else {
			LOG_DEBUG("Unhandled TCI command: %s",message.c_str());
		}
	}
}



void tci_binary(const std::vector<uint8_t>& message)
{
	const tciDataStream *rxAudio = reinterpret_cast<const tciDataStream*>(message.data());
	if (rxAudio->type == TxChrono)
	{
		//LOG_INFO("Received TxChrono packet");		
		// This is where we will send the buffered TX audio.
		guard_lock tci_lock(&mutex_tci_tx);
		if (tci_ptt && tci_txbuf.size() >= TCI_AUDIO_LENGTH)
		{
			// We have enough data to send TX audio
			tciDataStream *txAudio = new tciDataStream;
			txAudio->receiver=0;
			txAudio->sampleRate = audio_samplerate;
			txAudio->format = IqFloat32;
			txAudio->codec = 0u;
			txAudio->type = TxAudioStream;
			txAudio->crc = 0u;
			txAudio->length = TCI_AUDIO_LENGTH;
			std::copy(tci_txbuf.begin(),tci_txbuf.begin()+txAudio->length,txAudio->data);
			tci_txbuf.erase(tci_txbuf.begin(), tci_txbuf.begin()+txAudio->length);
			auto const ptr = reinterpret_cast<uint8_t*>(txAudio);
			std::vector<uint8_t> tx(ptr, ptr + sizeof(tciDataStream));
			ws->sendBinary(tx);
			delete txAudio;
		} else if (tci_ptt){
			LOG_DEBUG("TX Buffer is %d bytes, cannot send",tci_txbuf.size());
		}
		
	} else if (rxAudio->type == RxAudioStream && rxAudio->length > 0) {
		// We have the RX audio, need to add it to the local buffer for processing.
		//LOG_DEBUG("RX Audio receiver:%d samplerate:%d length:%d", rxAudio->receiver, rxAudio->sampleRate, rxAudio->length);
		guard_lock tci_lock(&mutex_tci_rx);
		tci_rxbuf.insert(tci_rxbuf.end(), rxAudio->data, rxAudio->data+rxAudio->length);
	}
}

void tci_audio_flush(unsigned dir)
{
	LOG_DEBUG("Flushing TCI %s audio",(dir==0)?"RX":"TX");
	if (dir == 0) {
		guard_lock tci_rx_lock(&mutex_tci_rx);
		tci_rxbuf.clear();	
		std::vector<float> ().swap(tci_rxbuf);// free memory
	} else {
		guard_lock tci_tx_lock(&mutex_tci_tx);
		tci_txbuf.clear();	
		std::vector<float> ().swap(tci_txbuf);// free memory
	}
}


int tci_audio_read(float* data, size_t len)
{
	guard_lock tci_lock(&mutex_tci_rx);
	int size = std::min(len,tci_rxbuf.size());
	std::copy(tci_rxbuf.begin(),tci_rxbuf.begin()+size,data);
	tci_rxbuf.erase(tci_rxbuf.begin(), tci_rxbuf.begin()+size);
	return size;
}

int tci_audio_write(float* data, size_t len)
{
	guard_lock tci_lock(&mutex_tci_tx);
	tci_txbuf.insert(tci_txbuf.end(), data, data+len);
	//if (tci_txbuf.size()>4096)
	//	LOG_DEBUG("TCI TX buffer growing: %d packets",tci_txbuf.size());	
	
	return len;
}

void tci_start_audio() {
	audio_required = true;
	if (ws != NULL && connected_to_tci) {
		guard_lock tci_lock(&mutex_tci);
		ws->send("AUDIO_START:0;");
		tci_audio_started=false;
	}		
}

void tci_stop_audio() {
	if (ws != NULL && connected_to_tci) {
		guard_lock tci_lock(&mutex_tci);
		ws->send("AUDIO_STOP:0;");
		tci_audio_started=false;
	}
}


void * tci_thread_loop(void *d)
{
	int poll = poll_interval;
	for (;;) {


		if  (ws != NULL && ws->getReadyState() == WebSocket::OPEN) {

			if (ws != NULL) {
				ws->poll(5);
				guard_lock tci_lock(&mutex_tci);
				if (ws->isBinary()) 
					ws->dispatchBinary(tci_binary);
				else
					ws->dispatch(tci_message);
			}

			if (connected_to_tci && poll_interval != progdefaults.tci_poll)
				poll_interval = progdefaults.tci_poll;

			if ((poll -= 10) <= 0) {
				poll = poll_interval;
				if (progdefaults.chkUSETCIis) {
					if (ws == NULL)
						connect_to_tci();
					else {
						if (trx_state == STATE_RX) {
							//tci_get_frequency();
							//tci_get_smeter();
							//tci_get_notch();
							//tci_get_wpm();
							//if (modes_posted) 
							//	tci_get_mode();
							//else
							//	tci_get_modes();

							//if (bws_posted)
							//	tci_get_bw();
							//else 
							//	tci_get_bws();
						}
						else {
							//tci_get_pwrmeter();
							//tci_get_wpm();
						}
					}
				}
			}
		} else {
			// Connection failed?
			LOG_INFO("TCI Disconnected");
			connected_to_tci=false;
			MilliSleep(1000);
			reconnect_to_tci();
			break;
		} 
	}
	ws->close();
	delete ws;
	ws = NULL;
	return NULL;
}

void reconnect_to_tci()
{
	if (ws != NULL)
	{
		ws->close();
		delete ws;
		ws = NULL;
	}

	connect_to_tci();

    connected_to_tci = false;
}

void tci_cwio_send_text(std::string s) {
	guard_lock tci_lock(&mutex_tci);
}


extern void tci_setwidth(pbwidth_t) {
	guard_lock tci_lock(&mutex_tci);
}

extern void tci_getwidth() {
	guard_lock tci_lock(&mutex_tci);
}
